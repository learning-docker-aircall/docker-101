# Dockerize me!

## What is Docker

In simple terms, Docker is a software platform that simplifies the process of building, running, managing and distributing applications. It does this by virtualizing the operating system of the computer on which it is installed and running.

![docker host](resources/docker_host.png "Docker Host")

The machine on which Docker is installed and running is usually referred to as a Docker Host or Host in simple terms.

So, whenever you plan to deploy an application on the host, it would create a logical entity on it to host that application. In Docker terminology, we call this logical entity a Container or Docker Container to be more precise.

A Docker Container doesn’t have any operating system installed and running on it. But it would have a virtual copy of the process table, network interface(s), and the file system mount point(s). These have been inherited from the operating system of the host on which the container is hosted and running.

Whereas the kernel of the host’s operating system is shared across all the containers that are running on it.

## What to do

### Install Docker

Simply download it from [here](https://docs.docker.com/desktop/mac/install/).

## Download your fist image

We will start easy: with a hello world!

```
docker pull hello-world
```

With this you will have an [image](https://docs.docker.com/glossary/#image) downloaded into your computer. If you want to run it you only have to do:

```
docker run hello-world
```

You always can check what images you have in your laptop using:

```
docker ps -a
```

## Create your first image

Create an image is easy, you only need to create a `Dockerfile` and build the image

```
docker build --tag dockerize .
```

and check if the image is created successfully:

```
docker images
```

If you want to run the image you only have to:

```
docker run dockerize
```

## Run multiple images at the same time

Usually you want to have multiple images working at the same time (for example a database, a server and a worker). So in this case you need to have multiple images and handle it with a `docker_compose.yml`. You have all the doc [here](https://docs.docker.com/compose/).

To execute it:
```
docker-compose up -d
docker-compose down
```

## Your turn!!

You have to create an image for javascript and add it to the docker compose to execute python and js tests in parallel. Good luck!


